# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'korm/version'

Gem::Specification.new do |s|
  s.name = 'korm'
  s.version = KORM::VERSION
  s.date = '2019-12-28'
  s.summary = 'Very simple sqlite3 ORM'
  s.authors = ['Imam Omar Mochtar']
  s.email = 'iomarmochtar@gmail.com'
  s.files = ['lib/korm.rb']
  s.license = 'MIT'
end
