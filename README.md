KORM
====

a sqlite3 CURD wrapper

Installation
------------

Set into your **Gemfile**:

```ruby
gem "korm", :git => "https://gitlab.com/iomarmochtar/korm.git", :branch => "devel"
```

Then run command to install it

```bash
bundle install
```

Usage
-----

You can set the path of sqlite file, if it's not specified then the date will be stored in memory, You also can set db path in env variable **DB_PATH**

```ruby
require 'korm'

db_path = '/tmp/data.sqlite3'
schemas = {
    'username' => 'string(100) not null',
    'gender' => 'boolean',
    'first_name' => 'string(100) not null',
    'last_name' => 'string(100)',
    'address' => 'text'
}

obj = KORM::Krudz.new 'employee', schemas, path: db_path
```

_Insert data_

```ruby
record_id = obj.insert({
    'username' => 'first',
    'gender' => 'true',
    'first_name' => 'Hello',
    'last_name' => 'To The World',
    'address' => 'Sukmajaya, Depok'
})
```

__Filter Chaining__

with **where** , **or_where**, **and_where**. Example for generate and execute sql
```sql
SELECT username, first_name
FROM employee
WHERE
    ( username LIKE 'fir%' AND gender == 'true' )
    OR
    ( address NOT LIKE '%Depok%' )
```

```ruby
rows = obj.select([ 'username', 'first_name' ])
          .where([
                [ 'username', KORM::C::LK, 'fir%' ],
                [ 'gender', KORM::C::EQ, 'true' ],
            ])
          .or_where([
               [ 'address', KORM::C::NL, '%Depok%' ]
          ])
          .all
```

_Update data_

```ruby
obj.where([[ 'id', KORM::C::EQ, 1 ]])
   .update({ 'username' => 'changed' })
```

_Get data by record id_

```ruby
row = obj.get(record_id)
```

_Get the first and last row_

```ruby
f_row = obj.where([[ 'address', KORM::C::LK, '%depok%' ]]).first()
l_row = obj.where([[ 'username', KORM::C::EQ, 'adhi' ]]).last()
```

_Delete data_

```ruby
obj.where([[ 'id', KORM::C::EQ, 1 ]]).delete
```

You also have access to execute raw query

```ruby
query = 'DELETE FROM x'
obj.execute(query)
```

Test
----

```bash
rake test
```
