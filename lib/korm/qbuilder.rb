# frozen_string_literal: true

module KORM

  # Query builder
  class QBuilder

    attr_reader :select, :where, :order, :vals

    # @param [Krudz] parent: object of krudz
    def initialize(table_name)
      @table_name = table_name
      reset
    end

    # (re)initialize parameters
    # @return [void]
    def reset
      @select = []
      @where = []
      @limit = 0
      @order = nil
      @vals = []
    end

    # Add criteria.
    #
    # @param [Array] filters: collection of criteria
    # @param [String] clause: The used clause for filters
    def add_where(filters, clause)
      q = []
      filters.each do |filter|
        field, comparator, value = filter

        q.push "#{field} #{comparator} ?"
        @vals.push value

      end

      clause = '' if @where.empty?
      @where.push "#{clause} ( #{q.join(' AND ')} )".strip
    end

    # Generating query for WHERE
    #
    # @return [String]
    def whereq
      @where.empty? ? '' : "WHERE #{@where.join(' ')}"
    end

    # Setter for fields that will be selected
    #
    # @return [void]
    def selector(fields = [])
      @select = fields
    end

    # Setter that used for ORDER BY
    #
    # @param [String] by: set field as ordering reference
    # @param [String] ordering: set ordering layout, ascending (ASC) or descending (DESC)
    def set_ordering(by:, ordering:)
      @order = "ORDER BY #{by} #{ordering}"
    end

    # SELECT query generator
    #
    # @param [Integer] limit: set limit of returned data/row
    # @return [Hash] returning query and value for prepared statement
    def selectq(limit: 0)
      selectc = @select.empty? ? '*' : @select.join(',')
      where = whereq
      limit = limit.equal?(0) ? '' : "LIMIT #{limit}"
      order = @order || ''
      query = [
        "SELECT #{selectc}",
        "FROM #{@table_name}",
        where,
        order,
        limit
      ]
      result = gen_return query
      reset
      result
    end

    # UPDATE query generator
    #
    # @param [Hash] data: data that will be change
    # @return [Hash] returning query and value for prepared statement
    def update(data)
      extract = KORM::Helpers.extract_data data
      changes = extract[:fields].map{ |field| "#{field}=?" }.join(',')
      where = whereq
      @vals = extract[:values] + @vals
      query = [
        "UPDATE #{@table_name}",
        "SET #{changes}",
        where
      ]
      result = gen_return query
      reset
      result
    end

    # DELETE query generator
    #
    # @return [Hash] returning query and value for prepared statement
    def delete
      where = whereq
      query = [
        "DELETE FROM #{@table_name}",
        where
      ]
      result = gen_return query
      reset
      result
    end

    private

    def gen_return(query)
      {
        query: KORM::Helpers.convert_q(query),
        vals: @vals
      }
    end

  end
end
