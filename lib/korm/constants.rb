# frozen_string_literal: true

module KORM

  # "or" clause for WHERE
  OR = 'OR'
  # "and" clause for WHERE
  AND = 'AND'

  # list of comparator
  module C
    # Equal
    EQ = '=='
    # Not equal
    NE = '!='
    # Less than
    LT = '<'
    # Greater equal
    LE = '<='
    # Greater than
    GT = '>'
    # Greater equal
    GE = '=>'

    # Like
    LK = 'LIKE'
    NL = 'NOT LIKE'
  end

  # ordering
  module ORDER
    DESC = 'DESC'
    ASC = 'ASC'
  end

end
