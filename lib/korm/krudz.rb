# frozen_string_literal: true

require 'sqlite3'
require 'pp'

module KORM

  # Simple Sqlite3 table CRUD operation
  class Krudz
    attr_accessor :db, :table_name

    # @param [String] table_name: set table name that will be operated
    # @param [Array] schema: table schema, optional if table already created
    # @param [String] path: Sqlite3 path. prioritizing env DB_PATH
    #                          otherwise the data will be set to memory
    # @param [SQLite3::Database] instance: use existing sqlite3 instance
    def initialize(table_name, schema, path: ':memory:', instance: nil)
      @db_path = path

      @table_name = table_name
      @schema = schema
      @db = instance || SQLite3::Database.open(path)
      @db.results_as_hash = true
      init_schema
      @qbuilder = QBuilder.new table_name
    end

    # Add chained delete clause.
    #
    # @param [Array] filters: Multi array for filter
    # @param [String] clause: set whether OR (or) AND
    # @return [Krudz]
    def where(filters, clause = KORM::OR)
      @qbuilder.add_where(filters, clause)
      self
    end

    # Where clause for AND filter
    #
    # @param [Array] filters: Multi array for filtering
    # @return [Krudz]
    def and_where(filters)
      @qbuilder.add_where(filters, KORM::AND)
      self
    end

    # Just a wrapper for where method
    # @return [Krudz]
    def or_where(filters)
      where filters
    end

    # Set select field that will be use
    #
    # @parm [Arrray] fields: set fields that will be use
    # @return [Krudz]
    def select(fields = [])
      @qbuilder.selector fields
      self
    end

    # Wrapper for search method to get all of data
    #
    # @return [Array[Hash]] all of data
    def all
      q = @qbuilder.selectq
      execute q[:query], q[:vals]
    end

    # Set the first record
    #
    # @return [Hash]
    def first
      order_by('id')
      q = @qbuilder.selectq limit: 1
      rows = execute q[:query], q[:vals]
      rows.first
    end

    # Set the last record
    #
    # @return [Hash]
    def last
      order_by('id', KORM::ORDER::DESC)
      q = @qbuilder.selectq limit: 1
      rows = execute q[:query], q[:vals]
      rows.first
    end

    # Set ordering
    #
    # @param [String] field: field that will referenced for ordering
    # @param [String] ordering: set ordering layout
    def order_by(field, ordering = KORM::ORDER::ASC)
      @qbuilder.set_ordering by: field, ordering: ordering
      self
    end

    # Insert data to table
    #
    # @param [Hash] data: data that will be inserted
    # @return [Integer] record id
    def insert(data)
      extract = KORM::Helpers.extract_data data
      marks = extract[:fields].map{ '?' }
      query = "INSERT INTO #{@table_name}
              (#{extract[:fields].join(',')})
              VALUES(#{marks.join(',')})"
      execute query, extract[:values]
      @db.last_insert_row_id
    end

    # Get data by record ID, a wrapper of search operation
    #
    # @param [Integer] id: record ID
    # @return [Array] row
    def get(id)
      search_param = ['id', KORM::C::EQ, id]
      and_where [search_param]
      first
    end

    # Delete data by record id
    #
    # @return [void]
    def delete
      q = @qbuilder.delete
      execute q[:query], q[:vals]
    end

    # Update data by record id
    #
    # @param [Hash] data: data that will be change
    # @return [void]
    def update(data)
      q = @qbuilder.update data
      execute q[:query], q[:vals]
    end

    # Execute SQL commands
    #
    # @param [String] query: query that will be executed
    # @param [Array] params: parameters for prepared statement
    # @return [Array] rows of query result (if any)
    def execute(query, params = [])
      # puts query
      # pp params
      @db.execute(query, params)
    end

    # create table schema, will be ignored if table already exists
    #
    # @return [void]
    def init_schema
      query = "CREATE TABLE IF NOT EXISTS #{@table_name}("
      # id  is a mandatory
      query += 'id INTEGER PRIMARY KEY AUTOINCREMENT,'
      fields = []
      @schema.each do |field, attr|
        fields.push "#{field} #{attr}"
      end
      query += "#{fields.join(',')})"

      execute query
    end

  end
end
