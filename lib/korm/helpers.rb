# frozen_string_literal: true

module KORM

  # Collection of utility function
  module Helpers
    # extracting hash into fields name, values, and "?" character
    #
    # @param [Hash] data: key-value data
    # @return [Hash] processed data, separated into fields and values
    def self.extract_data(data)
      {
        fields: data.keys,
        values: data.values
      }
    end

    def self.convert_q(query_lines)
      query = []
      query_lines.each do |line|
        line = line.strip
        query.push line unless line.empty?
      end
      query.join(' ')
    end
  end

end
