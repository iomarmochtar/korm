# frozen_string_literal: true

require 'fileutils'
require 'pp'

describe KORM::Krudz do
  schema = { 'name' => 'string(100) not null',
             'address' => 'text',
             'age' => 'integer(100) not null' }
  tbl_name = 'users'
  temporary_dir = 'tmp'
  test_data = [
    ['usman', 'Tebet Selatan', 20],
    ['korman', 'Tebet Selatan', 23],
    ['kris', 'Petojo Utara', 30],
    ['chacko', 'South India', 27]
  ]

  before :all do
    @test_dir = File.join(Dir.pwd, temporary_dir)
    @db_path = File.join(@test_dir, 'test.sqlite3')

    # clean up testing directory & set a temporary database file
    FileUtils.rm_rf(@test_dir) if File.directory?(@test_dir)
    Dir.mkdir(@test_dir)

    @obj = described_class.new tbl_name, schema, path: @db_path
  end

  context 'Database initalization' do
    it 'should use in memory storage if not set db path' do
      obj = described_class.new tbl_name, schema
      expect(obj.db.filename).to eq ''
    end

    it 'should create database file when db path is set' do
      expect(@obj.db.filename).to eq @db_path
    end

    it 'should create mentioned table' do
      q = 'SELECT name FROM sqlite_master WHERE name=? LIMIT 1'
      rows = @obj.db.execute q, [tbl_name]
      expect(rows.first['name']).to eq tbl_name
    end
  end

  context 'CRUD testing' do
    it 'should inserting the data' do
      test_data.each do |row|
        data = {  'name' => row[0],
                  'address' => row[1],
                  'age' => row[2] }
        @obj.insert(data)
      end
      expect(@obj.all.count).to eq test_data.count
    end

    it 'should retrieve the first column of first row' do
      expect(@obj.get(1)[1]).to eq test_data.first[0]
    end

    it 'should should return nil if get data by id is not exists' do
      expect(@obj.get(100)).to eq nil
    end

    it 'should retrieve data by criteria' do
      param = [['address', KORM::C::EQ, 'Tebet Selatan']]
      expect(@obj.where(param).all.count).to eq 2
    end

    it 'should be success for chained where [AND]' do
      crit1 = [['name', KORM::C::LK, '%man'],
               ['address', KORM::C::EQ, 'Tebet Selatan']]
      crit2 = [['age', KORM::C::LE, 25]]
      rows = @obj.select(['name'])
                 .where(crit1)
                 .and_where(crit2)
                 .all
      expect(rows.count).to eq 2
    end

    it 'should updating data' do
      change_to = 'alex'
      param = [
        ['id', KORM::C::EQ, 1]
      ]
      changep = { 'name' => change_to }
      @obj.where(param).update(changep)
      expect(@obj.get(1)['name']).to eq change_to
    end

    it 'should deleting data' do
      param = [['id', KORM::C::EQ, 3]]
      @obj.where(param).delete
      expect(@obj.all.count).to eq 3
    end

    it 'should able to direct executing sql command' do
      name = 'hola'
      query = "INSERT INTO #{tbl_name}(name, address, age) VALUES('#{name}', 'Amigos', 26)"
      @obj.execute(query)
      param = [['name', KORM::C::EQ, name]]
      expect(@obj.where(param).all.count).to eq 1
    end

  end

end
