# frozen_string_literal: true

describe KORM::QBuilder do
  before :all do
    @tbl = 'tbl'
    @obj = described_class.new @tbl
  end

  it 'should add where clause' do
    where1 = [
      ['field1', KORM::C::EQ, 'val1'],
      ['field2', KORM::C::NE, 'val2']
    ]
    @obj.add_where(where1, KORM::AND)

    expected = '( field1 == ? AND field2 != ? )'
    expect(@obj.where[0]).to eq expected
  end

  it 'should can be add with another where clause and has value for prepared statement' do
    where2 = [
      ['field1', KORM::C::LK, 'val1']
    ]
    @obj.add_where(where2, KORM::OR)
    expect(@obj.vals.count).to eq 3
  end

  it 'should can generating WHERE query' do
    expected = 'WHERE ( field1 == ? AND field2 != ? ) OR ( field1 LIKE ? )'
    expect(@obj.whereq).to eq expected
  end

  it 'should can set selector' do
    @obj.selector %w[field1 field2]
    expect(@obj.select.count).to eq 2
  end

  it 'should can generate query ordering by field' do
    @obj.set_ordering by: 'field1', ordering: KORM::ORDER::DESC
    expect(@obj.order).to eq 'ORDER BY field1 DESC'
  end

  it 'should can generating SELECT query' do
    q = @obj.selectq limit: 2
    expected = "SELECT field1,field2 FROM #{@tbl} WHERE ( field1 == ? AND field2 != ? ) OR ( field1 LIKE ? ) ORDER BY field1 DESC LIMIT 2"
    expect(q[:query].strip).to eq expected
  end

  it 'should can generating UPDATE query' do
    data = { 'field1' => 'hello' }
    @obj.add_where([['id', KORM::C::EQ, 1]], KORM::AND)
    q = @obj.update(data)
    expected = 'UPDATE tbl SET field1=? WHERE ( id == ? )'
    expect(q[:query].strip).to eq expected
  end

  it 'should can generating DELETE query' do
    @obj.add_where([['field1', KORM::C::LK, 'archive_%']], KORM::AND)
    q = @obj.delete
    expected = 'DELETE FROM tbl WHERE ( field1 LIKE ? )'
    expect(q[:query].strip).to eq expected
  end

end
