# frozen_string_literal: true

describe KORM do
  it 'has a version number' do
    expect(KORM::VERSION).not_to be nil
  end
end
